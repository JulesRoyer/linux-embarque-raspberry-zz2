#define _POSIX_C_SOURCE 201112L

#include "tcp.h"
#include "code.h"

#define ERROR_IF(F) if(F) { perror(#F); exit(errno); }

int tcp_connect(char *addr, char *port) {
	struct addrinfo hints, *res;
	int fd;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	ERROR_IF(getaddrinfo(addr, port, &hints, &res) != 0);
	ERROR_IF((fd = socket(res->ai_family, res->ai_socktype, 0)) == -1);
	ERROR_IF(connect(fd, res->ai_addr, res->ai_addrlen) == -1);

	return fd;
}

void controleVert(pthread_mutex_t * mutexVert, int lockVertR){ // Controle le clignotement de la led en prenant ou relachant la mutexVert
// Seul partie du programme qui gere la mutexVert
// Ne fait que ça donc fonctionnement parallele, recoit les commandes par le tube cote lecture lockVertR
	FILE * nonLus;
	char vert[2];
	char estLock=0;
	pthread_t LedV; // Thread qui permet la clignotement de la led des messages (verte)

	pthread_mutex_lock(mutexVert); // On verouille au demarrage
	estLock=1;
	
	sleep(3);
	
	pthread_create(&LedV, NULL, &clignoteV, mutexVert);
	// On verifie l'existence de messages non lus
	nonLus = fopen("/home/pi/Documents/nonLus.txt", "r");
	if(nbChar(nonLus) > 2) {// On a des messages non lus donc ca clignote
		pthread_mutex_unlock(mutexVert);
		puts("@ On a des messages non lus donc ca clignote");
		estLock=0;
	} 
	fclose(nonLus);

	puts("@ --------controleVert() en route");
	while(1){
		read(lockVertR, vert, 2); // On cherche a savoir si on veut eteindre le clignotement de la led verte ou au contraire l'activer
		if (vert[0] == '0' && !estLock) {
			pthread_mutex_lock(mutexVert); // On demande si possible d'arreter le clignotement
			estLock=1;
			puts("@ \tOn empeche la led verte de clignoter");
		} else if (vert[0] == '1') {
			pthread_mutex_unlock(mutexVert); // On a recu un nouveau message donc ça clignote
			estLock=0;
			puts("@ \tOn fait clignoter la led verte");
		}
	}
	pthread_cancel(LedV);
}

void recevoir(int fdTCP, int fdACK, int lockVertW, pthread_mutex_t * mutexNonLus, pthread_mutex_t * mutexTubeVert){
// Gere la reception tcp
// [leds] Ne peut qu'allumer la led rouge et faire clignoter la led verte 
	char buff[TAILLE_GROS_BUFFER]; // On prend tout un message (eventuellement) donc il faut de la place
	char petitbuff[6]; // On va prendre que le debut pour les tests : evite une recopie trop grande
	char ack[2];
	int total,i,espace; // Toujours rien a voir avec l'essence
	FILE * messages;
	FILE * nonLus;

	pthread_mutex_unlock(mutexNonLus); 
	
	puts("# --------recevoir() en route");
	while(1){

		puts("# Attente de quelque chose du serveur...");
		total = read(fdTCP, buff, TAILLE_GROS_BUFFER); // TODO : A changer avec timeout
		puts("# Ack ou recv recu du serveur");

		cleanStr(buff);

		for(i = 0; i<6; ++i){
			petitbuff[i] = buff[i];// On recopie le debut du gros buffer
		}

		if(estACK(petitbuff)){ // Si recu ack et pas recv
			puts("# Ack recu :");
			sprintf(ack, "%c", petitbuff[4]);
			write(fdACK, ack, 2); // On envoie la valeur du ack dans le tube vers le gestionnaire des envois / lecture des mails
			puts(ack);
		}
		else if(estRECV(petitbuff)){
			puts("# Recv recu");

			pthread_mutex_lock(mutexTubeVert);
			write(lockVertW, "1", 2); // Ca clignote parce qu'on a un nouveau message
			pthread_mutex_unlock(mutexTubeVert);
			
			pthread_mutex_lock(mutexNonLus); // On verifie que rien d'autre dans notre programme n'est deja en train de lire les fichiers
			puts("# >Ouverture de messages.txt et nonLus.txt");

			nonLus = fopen("/home/pi/Documents/nonLus.txt", "a"); // On va les ecrire a la fin pareil
			messages = fopen("/home/pi/Documents/messages.txt", "a"); // On va juste les ecrire a la fin 
			
			puts("# >Ecriture du message reçu");
			espace = 0;
			for(i = 5; i<total && buff[i] != '\0' && buff[i] != '\n'; ++i){ // On commence a 5 = la taille du mot "recv " et on ecrit les nouveaux messages
				fputc(buff[i], messages);
				fputc(buff[i], nonLus);
				if(!espace && buff[i] == ' '){
					fputs(": ", messages);
					fputs(": ", nonLus);
					espace = 1;
				}
					
			}
			fputc('\n', messages);
			fputc('\n', nonLus);
			
			fclose(nonLus);
			fclose(messages);
			puts("# >Fermeture nonLus.txt et messages.txt");
			pthread_mutex_unlock(mutexNonLus); // On en a fini avec eux, on les ferme et on rend la mutex, on peut donc les modifier dans envoyerLire() par exemple
			puts("# Fin operations apres recv");
		}
	}
}


void envoyerLire(int fdTCP, int fdUART, int fdACK, int lockVertW, pthread_mutex_t * mutexNonLus, pthread_mutex_t * mutexTubeVert){
// Gere l'envoi de commandes au serveur tcp et de commandes "internes" 
// telles que lire les messages non lus ou tous les messages, afficher son pseudo etc.
// [leds] Ne peut qu'eteindre la led rouge et allumer la led jaune
	char message[TAILLE_GROS_BUFFER];
	char destinataire[TAILLE_PSEUDO];
	char code[2]; // Code envoye par le client
	char nbMsg[10]; // nbMsg < 99 donc deux cases pour le nombre + une pour \0

	// Taille tableaux ci dessous = taille de message[] + taille de destinataire[] + ensuite les espaces et le mot "send" (ou l'absence de "recv")
	char commande[TAILLE_GROS_BUFFER + TAILLE_PSEUDO + 7]; // Comme ca on est sur de caser tout le monde (7 car taille de "send" + deux espaces + \0)
	char retourUART[TAILLE_GROS_BUFFER + TAILLE_PSEUDO + 2]; // On n'a pas le "recv" et un espace qui le suit 
												  // donc on enleve 5 caracteres par rapport a ce que l'on peut envoyer avec commande[]
	char retourUART2[TAILLE_GROS_BUFFER + TAILLE_PSEUDO + 2];
	char ack[2]; // Le numero du ack + \0
	char * sauvegarde; // Permet la sauvegarde de ce qu'il reste apres lecture du nb de messages demande
	int n, i, lu, total, tailleSauv, lignes, espace; 
	// n : nbMSG en int
	// i : Un indice de ligne
	// lu : nb de caracteres lus du fichier nonLus.txt et renvoyes vers le pc par uart
	// total : nb total de carateres du fichier nonLus.txt / de la chaine de caractere commande
	// tailleSauv : = total - lu
	// lignes : lignes totales presentes
	FILE * messages;
	FILE * nonLus;
	FILE * envoyes;
	puts("& --------envoyerLire() en route");
	while(1){
		puts("& ** Lecture/attente du code depuis l'uart **");
		readUART(fdUART, code, 2); // 1ere etape : on lit la "commande"
		// Bloquant donc des qu'on recoit = nouvelle commande
		toggleLed((couleurs_t) ERREUR, 0); // on fait une nouvelle action donc on eteint la led rouge de l'erreur
		puts("& ** Extinction led rouge de l'erreur **");
		switch(code[0]){

// 1 ==================== Envoi d'un message

			case('1'): // 1 pour send qui prend le destinataire puis le message complet a finir avec un \n
			puts("& \tEnvoi d'un nouveau message");
			
			strcpy(destinataire, ""); // On nettoie les chaines de caracteres
			strcpy(message, ""); 

			puts("& >Lecture destinataire");
			readUART(fdUART, destinataire, TAILLE_PSEUDO); 
			cleanStr(destinataire);
			puts("& >Lecture destinataire : CHECK");
			puts(destinataire);

			puts("& >Lecture message");
			readUART(fdUART, message, TAILLE_GROS_BUFFER);
			cleanStr(message);
			puts("& >Lecture message : CHECK");
			puts(message);
			
			toggleLed((couleurs_t) ENVOI, 1); // Une action est en cours
			puts("& >Allumage led jaune d'occupation");

			sprintf(commande, "send %s %s\n", destinataire, message); // On cree la bonne commande qui va bien
			write(fdTCP, commande, strlen(commande)); // On envoie en TCP
			
			total = strlen(commande);
			
			// On gere les erreurs avec la reponse du serveur, on ne charge pas plus le code
			read(fdACK, ack, 2); // attente du ack depuis le tube et erreur si mauvais ack
			if(ack[0] > '0'){ // Si mauvais ack alors allumage de la led rouge de l'erreur de la mort du deces qui tue
				toggleLed((couleurs_t) ERREUR, 1); // Erreur lors de l'envoi du message, veuillez recommencer
				puts("& >Erreur envoi nouveau message");
			}
			else {
				puts("& >Ouverture du fichier envoyes.txt");
				envoyes = fopen("/home/pi/Documents/envoyes.txt", "a");
				puts("& >Ecriture message envoye");
				espace = 0;
				for(i = 5; i<total; ++i){ // On commence a 5 = la taille du mot "send "
					fputc(commande[i], envoyes);
					if(!espace && commande[i] == ' '){
						fputs(": ", envoyes);
						espace = 1;
					}
				}

				fclose(envoyes);
				puts("& >Fin ecriture du message envoye");
				puts("& >Fermeture du fichier envoyes.txt");
			}
			ack[1] = '\n';
			writeUART(fdUART, ack); // On renvoie au client le ack, l'affichage est gere a son niveau

			toggleLed((couleurs_t) ENVOI, 0); // ack recu donc action terminee
			puts("& >Extinction led jaune d'occupation");

			puts("& \tFin envoi nouveau message");
			break;

// 2 ==================== Lecture des messages non lus

			case('2'): // 2 pour lire ses messages non lus, mais seulement le nombre qu'indique (<99)
			puts("& \tLecture des messages non lus");

			readUART(fdUART, nbMsg, 10); // Recuperer le nb de messages non lus a recuperer
			
			n = atoi(nbMsg);
			printf("& >Nombre de lignes demandees : %d\n", n);

			// On gere les erreurs ici car pas de retour ack du serveur dans cette partie
			if(n >= 0) { // 
				pthread_mutex_lock(mutexNonLus); // On verifie que rien d'autre dans notre programme n'est deja en train d'ecrire dans les fichiers
				puts("& >Ouverture du fichier nonLus.txt");
				// On compte le nombre de carateres du fichier pour gerer la sauvegarde
				lu = 0;
				nonLus = fopen("/home/pi/Documents/nonLus.txt", "r");
				total = nbChar(nonLus);

				rewind(nonLus);
				lignes = nbLignes(nonLus);

				// Si on a moins de lignes que demande, ou si n==0 on donne tous les messages non lus presents
				if(lignes < n || n == 0){
					pthread_mutex_lock(mutexTubeVert);
					write(lockVertW, "0", 2); // On arrete de clignoter
					pthread_mutex_unlock(mutexTubeVert);
					n = lignes;
				}
				
				printf("& >Nombre de lignes que le client s'attend a recevoir : %d\n", n);
				sprintf(retourUART, "%d\n", n);
				writeUART(fdUART, retourUART); // On envoie le nb de lignes que le client va s'attendre à recevoir
				puts("& >Nombre envoye vers uart");
				//Lecture du nombre de lignes demandees et extraction du contenu, envoi vers le client (le nombre de fois demande)
				rewind(nonLus);
				for (i=0; i<n; ++i){ // On prend chaque ligne et on envoie vers l'ordinateur
					usleep(10000);
					// TAILLE_GROS_BUFFER + 32 + 1 et non pas + 2 car on garde une place pour le \0
					fgets(retourUART2, (TAILLE_GROS_BUFFER + 32 + 1), nonLus); // Lit ligne par ligne, on s'arrete a \n ou a \0
					cleanStr(retourUART2);

					sprintf(retourUART, "%s\n", retourUART2);
					writeUART(fdUART, retourUART);
					puts("& >Message envoye vers uart :");
					puts(retourUART);

					lu += strlen(retourUART);
					strcpy(retourUART, "");
					
					readUART(fdUART, retourUART2, 3);
					strcpy(retourUART2, "");
				}

				// Sauvegarde du contenu non lu du fichier "nonLus.txt"
				tailleSauv = total - lu;
				sauvegarde = (char*)malloc((tailleSauv + 1)*sizeof(char)); // +1 pour le \0
				
				for (i=0; i<tailleSauv; ++i){	sauvegarde[i] = fgetc(nonLus);	} // On sauvegarde le contenu pas lu
				sauvegarde[i] = '\0';
				fclose(nonLus);

				nonLus = fopen("/home/pi/Documents/nonLus.txt", "w"); // On fait un nouveau vide
				fprintf(nonLus, "%s", sauvegarde); // Et on met le reste de l'ancien dans le nouveau
				fclose(nonLus);
				// Fin sauvegarde

				nonLus = fopen("/home/pi/Documents/nonLus.txt", "r");
				if(nbChar(nonLus) == 0) write(lockVertW, "0", 2); // On n'a plus de message non lu donc ca ne clignote plus
				fclose(nonLus);

				puts("& >Fermeture du fichier nonLus.txt");
				pthread_mutex_unlock(mutexNonLus); // On a fini d'utiliser le fichier nonLus.txt
				free(sauvegarde);
			}
			else {
				puts("& >Erreur, nombre incorrect, reessayez");
				sprintf(retourUART, "Erreur, nombre incorrect, reessayez\n");
				toggleLed((couleurs_t) ERREUR, 1); // Erreur lors de l'envoi du message, veuillez recommencer
				writeUART(fdUART, retourUART);
				puts("& >Erreur envoyee vers uart");
			}

			puts("& \tFin lecture des messages non lus");
			break;

// 3 ==================== Lecture de tous les messages

			case('3'): // 3 pour lire tous les messages, a voir si on dit qu'il n'y a plus de messages non lus
			puts("& \tLecture de tous les messages");

			pthread_mutex_lock(mutexNonLus); // On verifie que rien d'autre dans notre programme n'est deja en train d'ecrire dans les fichiers
			puts("& >Ouverture du fichier messages.txt");

			messages = fopen("/home/pi/Documents/messages.txt", "r");
			lignes = nbLignes(messages);
			printf("& >Nombre de lignes que le client s'attend a recevoir : %d\n", lignes);

			sprintf(retourUART, "%d\n", lignes);
			writeUART(fdUART, retourUART); // On envoie le nb de lignes que le client va s'attendre à recevoir
			puts("& >Nombre envoye vers uart");
			
			rewind(messages);

			for (i=0; i<lignes; ++i) {
				usleep(10000);
				fgets(retourUART2, (TAILLE_GROS_BUFFER + 32 + 1), messages); // Lit ligne par ligne, on s'arrete a \n
				cleanStr(retourUART2);

				sprintf(retourUART, "%s\n", retourUART2);
				writeUART(fdUART, retourUART);
				puts("& >Message envoye vers uart :");
				puts(retourUART);
				strcpy(retourUART, "");
				
				readUART(fdUART, retourUART2, 3);
				strcpy(retourUART2, "");
			}

			puts("& >Ecrasement de nonLus.txt");
			nonLus = fopen("/home/pi/Documents/nonLus.txt", "w"); // On fait un nouveau vide
			fclose(nonLus);
			fclose(messages);
			puts("& >Fermeture de nonLus.txt et message.txt");

			pthread_mutex_lock(mutexTubeVert);
			write(lockVertW, "0", 2); // On n'a plus de message non lu donc ca ne clignote plus
			pthread_mutex_unlock(mutexTubeVert);
			
			puts("& >On n'a plus aucun message non lu");

			pthread_mutex_unlock(mutexNonLus); // On a fini d'utiliser le fichier nonLus.txt
			puts("& \tFin lecture de tous les messages");
			break;

// 4 ==================== Lecture des messages envoyes

			case('4'):
			puts("& \tLecture des messages envoyes");

			envoyes = fopen("/home/pi/Documents/envoyes.txt", "r");
			lignes = nbLignes(envoyes);
			printf("& >Nombre de lignes que le client s'attend a recevoir : %d\n", lignes);

			sprintf(retourUART, "%d\n", lignes);
			writeUART(fdUART, retourUART); // On envoie le nb de lignes que le client va s'attendre à recevoir
			puts("& >Nombre envoye vers uart");
			
			rewind(envoyes);

			for (i=0; i<lignes; ++i) {
				usleep(10000);
				fgets(retourUART2, (TAILLE_GROS_BUFFER + TAILLE_PSEUDO + 1), envoyes); // Lit ligne par ligne, on s'arrete a \n
				cleanStr(retourUART2);

				sprintf(retourUART, "%s\n", retourUART2);
				writeUART(fdUART, retourUART);
				puts("& >Message envoye vers uart :");
				puts(retourUART);
				strcpy(retourUART, "");
				
				readUART(fdUART, retourUART2, 3);
				strcpy(retourUART2, "");
			}
			
			fclose(envoyes);
			
			puts("& \tFin lecture des messages envoyes");
			break;

// 5 ==================== Choix / Changement de pseudo

			case('5'):
			puts("& \tChangement/choix de pseudo");
			strcpy(destinataire, "");

			readUART(fdUART, destinataire, TAILLE_PSEUDO); // On recycle destinataire pour stocker le pseudo
			puts("& >Pseudo recu de l'uart :");
			cleanStr(destinataire);
			
			puts(destinataire);

			sprintf(commande, "nick %s\n", destinataire);
			write(fdTCP, commande, strlen(commande)); // On envoie en TCP
			puts("& >commande envoyée:");
			puts(commande);
			puts("& >Envoi au serveur du nickname");

			read(fdACK, ack, 2); // attente du ack depuis le tube et erreur si mauvais ack
			if(ack[0] > '0'){ // Si mauvais ack alors allumage de la led rouge de l'erreur de la mort du deces qui tue
				toggleLed((couleurs_t)ERREUR, 1); // Erreur lors de l'envoi du message, veuillez recommencer
				puts("& >Erreur choix de pseudo");
			}

			ack[1] = '\n';
			writeUART(fdUART, ack); // On renvoie au client le ack, l'affichage est gere a son niveau
			puts("& >Envoi de l'ack vers uart");

			puts("& \tFin changement/choix de pseudo");
			break;
		}
	}
}

void cleanStr(char * string){
	int i = 0;
	while (string[i] != '\0' && string[i] != '\n'){
		++i;
	}
	string[i] = '\0';
	puts(">>Chaine nettoyée");
}

int nbLignes(FILE * fichier){
	int nb = 0;
	char c;
	while(!feof(fichier)){
		c=fgetc(fichier);
		if(c=='\n') ++nb;
	}
	return nb;
}

int nbChar(FILE * fichier){
	int nb = 0;
	while(!feof(fichier)){
		fgetc(fichier);
		++nb;
	}
	return nb;
}

int estACK(char * msg){
	int i = 0; char ack[4] = "ack";
	while(msg[i] == ack[i]){++i;}
	return i>=3; // retourne vrai si msg[] = "ack"
}

int estRECV(char * msg){
	int i = 0; char recv[5] = "recv";
	while(msg[i] == recv[i]){++i;}
	return i>=4; // retourne vrai si msg[] = "recv"
}
