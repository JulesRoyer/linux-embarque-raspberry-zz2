#ifndef LED_H
#define LED_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

typedef enum {
	ERREUR = 16,
	MESSAGE = 20,
	ENVOI = 21
} couleurs_t;

void construireLeds(couleurs_t nom[], int taille);
void detruireLeds(couleurs_t nom[], int taille);
int toggleLed(int nom, int x);
void * clignoteV(void * mutex);
void demarrage(couleurs_t * leds, int taille);

#endif