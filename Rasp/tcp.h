#ifndef TCP_H
#define TCP_H

#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include "uart.h"
#include "led.h"

/*
 * This function is to help you tu connect to the server.
 * Its parameters are an IP address, like "172.16.32.27" and a port, like "1337"
 * It returns a file descriptor that you can use with open(2), write(2), close(2), ... (see `man 2 open`, etc)
 * If it returns -1, that means it has failed
 */
int tcp_connect(char *addr, char *port);
void controleVert(pthread_mutex_t * mutexVert, int lockVertR);
void recevoir(int fdTCP, int fdACK, int lockVertW, pthread_mutex_t * mutexNonLus, pthread_mutex_t * mutexTubeVert);
void envoyerLire(int fdTCP, int fdUART, int fdACK, int lockVertW, pthread_mutex_t * mutexNonLus, pthread_mutex_t * mutexTubeVert);
int estACK(char * msg);
int estRECV(char * msg);
int nbChar(FILE * fichier);
int nbLignes(FILE * fichier);
void cleanStr(char * string);

#endif