#include "led.h"
#include "tcp.h"
#include "uart.h"
#include <fcntl.h>

//Ce code est fourni sans les accents pour eviter toute erreur

#define TAILLEL 3
#define ADRESSE "192.168.99.160"
#define PORT "1337"

int main() {
	//Definitions
	couleurs_t leds[] = {ENVOI, MESSAGE, ERREUR}; // On peut diminuer ou agrandir si le nombre de leds change
	pthread_mutex_t mutexVert; //On donne la mutex pour faire clignoter la led verte
	pthread_mutex_t mutexNonLus; // Mutex qui va nous permettre d'eviter d'ouvrir nonLus.txt et messages.txt en meme temps que l'autre fork
	pthread_mutex_t mutexTubeVert; // Permet que recevoir() et envoyerLire() n'ecrivent pas en même temps sur le tube lockVert
	int fdTCP, fdUART;
	int fdACK[2]; // Tube pour faire passer les ack de recevoir() vers envoyerLire()
	int lockVert[2]; // Tube pour faire passer les demandes d'arret de clignotement de led de envoyerLire() vers recevoir()

	// On s'assure que les fichiers existent
	FILE * mxxx;
	mxxx = fopen("/home/pi/Documents/messages.txt", "a");
	fclose(mxxx);
	mxxx = fopen("/home/pi/Documents/nonLus.txt", "a");
	fclose(mxxx);
	mxxx = fopen("/home/pi/Documents/envoyes.txt", "a");
	fclose(mxxx);
	
	//Initialisations
	pthread_mutex_init(&mutexVert, NULL);
	pthread_mutex_init(&mutexNonLus, NULL);
	pthread_mutex_init(&mutexTubeVert, NULL); 
	
	pthread_mutex_unlock(&mutexNonLus);
	pthread_mutex_unlock(&mutexTubeVert);
	
	construireLeds(leds, TAILLEL); // On cree toutes les leds au niveau gpio
	
	pipe(fdACK);
	pipe(lockVert);

	demarrage(leds, TAILLEL); // Petite sequence de demarrage

	fdUART = uart_open("/dev/ttyS0"); // Tentative d'ouverture de l'uart
	puts((fdUART<0) ? "Pas UART" : "UART ok");
	
	fdTCP = tcp_connect(ADRESSE, PORT); // Tentative de connexion tcp
	puts((fdTCP<0) ? "Pas TCP" : "TCP ok");

	//La suite (gestion des envois et receptions)
	//Possible si le tcp_connect et le uart_connect a fonctionne
	if( fdUART>=0 && fdUART>=0 ){
	//----------------------------------------------------- Gestion des messages
		puts("Fork 1");
		puts("Code de log :\n\t# : recevoir(), communication TCP serveur-raspberry\n\t& : envoyerLire(), communication UART raspberry-pc\n\t@ : controleVert(), controle le clignotement de la led verte");
		if(fork() > 0) { //Gestion des lectures
			puts("Fork 2");
			if(fork() > 0){
				recevoir(fdTCP, fdACK[1], lockVert[1], &mutexNonLus, &mutexTubeVert); // on recoit des ack et des recv ici
			}
			else {
				controleVert(&mutexVert, lockVert[0]); // on controle le clignotement de la led verte par mutex
			}
		}
		else { // Gestion des ecritures
			envoyerLire(fdTCP, fdUART, fdACK[0], lockVert[1], &mutexNonLus, &mutexTubeVert); // on envoie des messages recus par l'uart ici
		}
	} else {
		puts("Erreur ouverture TCP/UART, impossible d'etablir l'environnement favorable du programme\nFermeture...");
	}

	detruireLeds(leds, TAILLEL);// On efface les traces
	return 0;
}
