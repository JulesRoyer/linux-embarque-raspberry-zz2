#include "led.h"

#define EXPORT "/sys/class/gpio/export"
#define UNEXPORT "/sys/class/gpio/unexport"
#define GPIO "/sys/class/gpio/gpio"
#define REPONSE 42

void construireLeds(couleurs_t nom[], int taille) { //Permet l'accès au GPIO pour les 3 leds
	FILE * activLed;
	char chemin[255];
	int i;
	for(i=0; i<taille; ++i){
		activLed = fopen(EXPORT, "w");
		if(activLed){
			fprintf(activLed, "%d", nom[i]);
			fclose(activLed);
		}
	}
	sleep(1);
	for(i=0; i<taille; ++i){
		sprintf(chemin, "%s%d/direction", GPIO, nom[i]);
		
		activLed = fopen(chemin, "w");
		if(activLed){
			fprintf(activLed, "out");
			fclose(activLed);
		} else printf("Pas out pour led gpio numero %d\n", nom[i]);
	}
}

void detruireLeds(couleurs_t nom[], int taille) { //Efface les traces
	FILE * activLed;
	int i;
	
	for(i=0; i<taille; ++i){
		activLed = fopen(UNEXPORT, "w");
		if(activLed){
			fprintf(activLed, "%d", nom[i]);
			fclose(activLed);
		}
	}
}

int toggleLed(int nom, int var){ // On allume ou on eteind une led
	int code = 0; //1 si ok 0 sinon
	FILE * activLed;
	char chemin[255];
	
	sprintf(chemin, "%s%d/value", GPIO, nom);
	activLed = fopen(chemin, "w");
	if(activLed){
		fprintf(activLed, "%d", var);
		fclose(activLed);
		code = 1;
	}
	return code;
}

void * clignoteV(void * m){ //Fait clignoter la led verte (c'est la seule qui clignote)
	pthread_mutex_t * mutex=(pthread_mutex_t *)m;
	while(1){
		pthread_mutex_lock(mutex);
		
		toggleLed((couleurs_t)MESSAGE, 1);
		
		pthread_mutex_unlock(mutex);
		
		usleep(500000); //500 ms
		
		toggleLed((couleurs_t)MESSAGE, 0);
		
		usleep(500000); //500 ms
	}
}

void demarrage(couleurs_t * leds, int taille){ // mini animation de demarrage
	int i,j;
	for(j = 1; j >= 0; --j){
		for(i = 0; i < taille; ++i){
			toggleLed(leds[i], j);
			usleep(100000);
		}
		sleep(1);
	}
}
