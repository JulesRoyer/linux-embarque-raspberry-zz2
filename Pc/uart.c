#define _DEFAULT_SOURCE

#include "uart.h" 

int uart_open(char const*filename) {
	int speed = B115200;

	struct termios options;
	int fd = open(filename, O_RDWR);
	if(fd < 0) return fd;

	fcntl(fd, F_SETFL, 0);
	tcgetattr(fd, &options);
	usleep(10000);
	cfsetospeed(&options, speed);
	cfsetispeed(&options, speed);
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
	options.c_oflag &= ~CRTSCTS;
	options.c_oflag &= ~OPOST;
	options.c_lflag &= ~(ICANON|ECHO|ECHONL|IEXTEN|ISIG);

	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 40;

	tcsetattr(fd, TCSANOW, &options);
	tcflush(fd, TCIOFLUSH);
	usleep(10000);

	return fd;
}

void readUART(int fd, char * dest, int taille){ // Pourquoi faire simple quand on peut faire compliqué, mais au moins ça marche nickel
	char buffer[10];
	char grosBuffer[TAILLE_GROS_BUFFER];
	int total = 0; // Rien à voir avec l'essence 	
	int nb = 1;

	// On inititalise nb et le buffer ici pour rentrer dans la boucle 		
	buffer[0]='a';

	while(total < TAILLE_GROS_BUFFER && buffer[nb-1]!='\n' && buffer[nb-1]!='\0'){ // <--ici, la boucle 			
		nb=read(fd,buffer,10);
		 						
		if(nb+total > TAILLE_GROS_BUFFER){ // Si on s'apprête à dépasser, on n'écrit que jusqu'à remplir le buffer 				
			nb=TAILLE_GROS_BUFFER-total;
		}

		strncpy(grosBuffer+total,buffer,nb); 			
		total+=nb;	
	}

	strncpy(dest, grosBuffer, taille);
}

void writeUART(int fd, char * src){
	write(fd, src, strlen(src));
}
