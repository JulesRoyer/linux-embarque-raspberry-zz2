#include "uart.h"
#include "code.h"
#include <stdlib.h>
#define UART_ADR "/dev/ttyUSB0"


//--------------------Fonctions de communication avec la Rasp----------------------

void acquittement(int fd){
	char ack[2];
	readUART(fd,ack,2);
	printf("\n");
	switch (ack[0]){
		case '0': //succès
			printf("Envoi au serveur réussi !\n");
			break;
		case '1': //commande inconnue
			fprintf(stderr,"Erreur : commande inconnue\n");
			break;
		case '2': //parse error
			fprintf(stderr,"Erreur : parse error\n");
			break;
		case '3': //nickname unavailable
			fprintf(stderr,"Erreur : pseudo non disponible\n");
			break;
		case '4': //invalid nickname
			fprintf(stderr,"Erreur : pseudo invalide\n");
			break;
		case '5': //nickname required
			fprintf(stderr,"Erreur : pseudo requis\n");
			break;
		case '6': //unknown recipient
			fprintf(stderr,"Erreur : destinataire inconnu\n");
			break;
		case '7': //invalid message
			fprintf(stderr,"Erreur : message invalide\n");
			break;
		default : //???????????????????????????????????????????????????????!????????????????????????
			fprintf(stderr,"Erreur inconnue, code : %c\n",ack[0]);
			break;
	}
}

void envoi(int fd, char* buffer){
	char pseudo[TAILLE_PSEUDO];
	writeUART(fd,CODE_ENVOI);
	printf("entrer destinataire :\n");
	fgets(pseudo,TAILLE_PSEUDO,stdin);
	writeUART(fd,pseudo);
	
	printf("entrer message :\n");
	fgets(buffer,TAILLE_GROS_BUFFER,stdin);	
	writeUART(fd,buffer);
	
	acquittement(fd);
}

void cleanBuffer(char * buffer){
	int i=0;
	while(buffer[i] != '\n') ++i;
	buffer[i]='\0';
}

void lectureMess(int fd, char * buffer){
	int i, nb;
	readUART(fd,buffer,TAILLE_GROS_BUFFER);	
	nb=atoi(buffer);
	
	if(!nb){
		printf("Aucun message de ce type\n");
	} else {
		printf("On s'attend a avoir %d messages\n", nb);
		for(i=1;i<=nb;i++){
			readUART(fd,buffer,TAILLE_GROS_BUFFER);
			cleanBuffer(buffer);			
			printf("%d\t- %s\n",i,buffer);
			//aquittement de la réception du message
			writeUART(fd,"1\n");
		}
		printf("-----Fin des messages-----\n");
	}
	strcpy("", buffer);
}

void lectureNonLu(int fd, char *buffer){
	char n[4];
	int nb;
	char nbMess[10];
	writeUART(fd, CODE_MSG_NON_LU);
	
	printf("Combien de messages voulez-vous lire ? (0 pour tous les messages)\n");
	fgets(n, 4, stdin);
	nb = atoi(n);
	if(nb<0) nb=1; //si l'utilisateur est un gros faisan, on répare ses âneries
	sprintf(nbMess,"%d\n",nb);
	writeUART(fd,nbMess);
	
	printf("%d premiers messages non lus :\n",nb);		
	lectureMess(fd,buffer);
}

void lectureTous(int fd, char * buffer){
	writeUART(fd, CODE_TOUS_MSG);
	lectureMess(fd, buffer);
}

void lectureEnvoyes(int fd, char*buffer){
	writeUART(fd, CODE_MSG_ENVOYES);
	lectureMess(fd, buffer);
}


void envoiPseudo(int fd, char * pseudo){
	writeUART(fd, CODE_PSEUDO);

	usleep(1000);
	writeUART(fd,pseudo);
	acquittement(fd);
}

void changerPseudo(int fd, char *pseudo){
	
	printf("entrer nouveau pseudo :\n");
	fgets(pseudo,TAILLE_PSEUDO,stdin);
	envoiPseudo(fd,pseudo);
}

void initPseudo(int fd, char* pseudo){
	char c[4];
	//TODO : stocker dans un fichier, éventuellement...
	printf("Pseudo actuel : %s\nChanger de pseudo ? (o,n)\n",pseudo);
	fgets(c, 4, stdin);
	if (c[0]=='o' || c[0]=='O') {
		changerPseudo(fd,pseudo);
	} else {
		envoiPseudo(fd,pseudo);		
	}
}

void quitter(int fd){
	writeUART(fd, CODE_FIN);
}


//--------------------Menu----------------------
char menu(){
	char choix[4];
	printf("\nEntrez votre choix :\n");
	printf("1 : Envoyer un message\n");
	printf("2 : Lire les messages non lus\n");
	printf("3 : Lire tous les messages\n");
	printf("4 : Lire les messages envoyés\n");
	printf("5 : Changer de pseudo\n");
	printf("p : Afficher le pseudo actuel\n");
	printf("q : quitter\n");
	fgets(choix, 4, stdin);
	return choix[0];
}


//--------------------Programme principal----------------------
int main(){
	
	char c, buffer[TAILLE_GROS_BUFFER];
	char pseudo[TAILLE_PSEUDO]="xxXD4rkP0n3yS4tan666OfTh3De4dzZXxx\n";
	
	int fd = uart_open(UART_ADR);
	
	if(fd != -1){
		
		initPseudo(fd, pseudo);
		
		do {
			c=menu();
			switch(c){
				case '1':
					envoi(fd,buffer);
					break;
				case '2':
					lectureNonLu(fd,buffer);
					break;
				case '3':
					lectureTous(fd,buffer);
					break;
				case '4':
					lectureEnvoyes(fd,buffer);
					break;
				case '5':
					changerPseudo(fd,pseudo);
					break;
				case 'p':
					puts(pseudo);
					break;
				default:
					break;
			}			
		} while(c!='q');
		
		quitter(fd);		
		close(fd);
		
	} else { //Erreur lors de l'ouverture de l'uart
		fprintf(stderr, "Erreur d'ouverture uart, veuillez réessayer en étant Root\n");
	}
	
	return 0;
}
