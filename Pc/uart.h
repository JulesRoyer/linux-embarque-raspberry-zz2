#ifndef UART_H
#define UART_H

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h> 
#include "uart.h" 
#include <string.h> 

//"de gros messages impliquent un gros buffer" - Tony Stark 
#define TAILLE_GROS_BUFFER 8000

/*
 * This function is to help you to open the UART connection between the PC and the Raspberry Pi
 * Its parameter is the UART device file, like "/dev/ttyAMA0" (ou ttyS0 ?) (pi) or "/dev/ttyUSB0" (pc)
 * It returns a file descriptor that you can use with open(2), write(2), close(2), ... (see `man 2 open`, etc)
 * If it returns -1, that means it has failed
 */
int uart_open(char const*filename);
void readUART(int fd, char * dest, int taille);// Fonctions a nous
void writeUART(int fd, char * src);// pour communiquer avec l'uart

#endif
